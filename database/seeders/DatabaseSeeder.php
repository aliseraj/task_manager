<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $admin = User::create([
            'name'=>'super admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('password'),
            'api_token' => Str::random(60),

        ]);

        $user1 = User::create([
            'name'=>'user1',
            'email'=>'user1@gmail.com',
            'password'=>bcrypt('password'),
            'api_token' => Str::random(60),

        ]);

        $proj = Project::create([
            'title'=>'title project 1',
            'description'=>'descriptionasdadadadadada project',
            'manager_id'=>$admin->id,
        ]);

        $task1 = Task::create([
            'title'=>'title task 1 ',
            'description'=>'description task 1 asdadadadasdasda',
            'user_id'=>$admin->id,
            'project_id'=>$proj->id,
            'status_code'=>'COMP',

        ]);
        $task2 = Task::create([
            'title'=>'title task 2 ',
            'description'=>'description task 2 asdadadadasdasda',
            'user_id'=>$user1->id,
            'project_id'=>$proj->id,
            'status_code'=>'OPEN',

        ]);

        $proj->users()->saveMany([$admin,$user1]);
    }
}
